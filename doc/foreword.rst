Foreword
========

How Kaylee Was Born
-------------------

I first met Python in 2008. At the same time I was keen on volunteer computing.
`Search for Extraterrestrial Intelligence <SETI>`_
seemed a bit science-fiction alike but thousands PC owners donated their
computers' computational resources to analyze the signals out of Space.
The `BOINC`_ platform on which `SETI@home` is built required the users to
download a client and leave it on (or use it as a screensaver).

But what if you don't want or not allowed to install the required software?
What if your platform is not supported?
Wouldn't it be wonderful if a person just opened a page in browser and a
second later the computing had started? Moreover, wouldn't it be great if
you've been browsing your favourite news site, while the brwoser was looking
for the HIV or cancer cure in background

With those ideas in mind I started developing a Python/JavaScript framework
but...
But I guess it was too early for me, as I was just learning Python and
although the code had some good ideas in it, it was ugly and unmaintainable.

Four years passed and I have returned to the idea of browser-based volunteer
computing. Let me describe the situation, as it is in year 2012:

* PCs are still alive but people prefer laptops to the old-fashioned towers.
* Dual-core processors in top-line smartphones is a usual thing and
  everything moves towards quad-core gadgets.
* Tablets are very popular and their performance is somewhere between
  laptops and smartphones.

As to technologies:

* The HTML5 standard is getting more and more support in the major browsers
  both on desktop and mobile platforms.
* The standard includes so-called Web Workers which allow executing the
  code in a separate thread, without interfering with the browsers' main
  javascript loop.

It seemed a right time to give life to the ideas described above.

Why *"Kaylee"*? If you have ever watched the "`Serenity`_" series, you would
figure the answer right away. The beautiful *Kaywinnet Lee "Kaylee" Frye* 
portrayed by Jewel Staite is a mechanical genius who keeps the ship running. 
Given the time she can deal with any problem that occurs on a ship.

I hope that the ``Kaylee framework`` will be able to maintain all kind of
distributed/volunteer computing projects and do it with the same elegance as
*Kaylee* fixes the *Serenity*.


Where should you go from here? I suggest continuing with :ref:`installation` or
:ref:`firststeps`.

.. _BOINC: http://boinc.berkeley.edu/
.. _SETI: http://setiathome.berkeley.edu/
.. _Serenity: http://www.imdb.com/title/tt0379786/
