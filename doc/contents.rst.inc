User's Guide
------------

This part of the documentation, which is mostly prose, begins with some
background information about Kaylee, then focuses on step-by-step
instructions for deployment of Kaylee.

.. toctree::
   :maxdepth: 2

   foreword
   installation
   firststeps
   tutorial/index
   loading
   default-communication
   auto_filters
   contrib

API Reference
-------------

If you are looking for information on a specific function, class or
method, this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   clientapi
   serverapi

.. toctree::
   :maxdepth: 1

   config

Additional Notes
----------------

..
   Design notes, legal information and changelog are here for the interested.

.. toctree::
   :maxdepth: 1

   license
   authors
