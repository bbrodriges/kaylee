.. _tutorial:

Tutorial
========
The easiest way to start with Kaylee is to dive into a simple distributed
computing application. In this tutorial we are going to write an application
which computes the value of PI via the **Monte Carlo method**.
Before starting the new application please make sure that you are able to
run Kaylee :ref:`demo` which will guarantee that you will be able to run
the tutorial application.

In case you'd like to get the whole source code of the tutorial application
at once, visit the following repository:
http://github.com/BasicWolf/kaylee-tutorial-app

.. toctree::
   :maxdepth: 1

   introduction
   requirements
   structure
   client_side
   server_side
   configuration
   compiling
   running
