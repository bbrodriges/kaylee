v0.2
----
* No need for permanent storage for apps like HashCracker.


v0.1 <-- current
----

. Rename "task results" -> "task solutions"
+ Modify Storages API, so that PermanentStorage and TemporalStorage
  interfaces look the same.
